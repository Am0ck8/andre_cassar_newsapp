package com.example.newsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Pinview pinview = (Pinview) findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                Toast.makeText(MainActivity.this, ""+pinview.getValue(), Toast.LENGTH_SHORT).show();
                if (pinview.getValue().equalsIgnoreCase("1234"))
                {
                    Toast.makeText(MainActivity.this, "Correct Pin", Toast.LENGTH_SHORT).show();
                    openActivity2();
                }
                else{
                    Toast.makeText(MainActivity.this, "Incorrect Pin", Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < pinview.getPinLength(); i++) {
                        pinview.onKey(pinview.getFocusedChild(), KeyEvent.KEYCODE_DEL, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));
                    }
                }
            }
        });
    }

    public void openActivity2()
    {
        Intent intent = new Intent(this, com.example.newsapp.HomePage.class);
        //Intent intent = new Intent(this, com.example.newsapp.GoogleAuth.class);
        startActivity(intent);
    }

}

package com.example.newsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Search extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    Spinner spinValue;
    static ArrayList<RecyclerItem> eList = new ArrayList<>();
    private RecyclerView myRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static int SPINNER;
    public static String date;
    public static String TEXT = "text";

    private int pos;

    String newsUrl;

    //ewList.add(new RecyclerItem(R.drawable.ic_a))
    //String[] categories = { "business", "entertainment", "general", "health", "science", "sports", "technology" };
    //String[] countries = { "ar", "de", "en", "es", "fr", "he", "it", "nl", "no", "pt", "ru", "se", "ud", "zh" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
         //   @Override
       //     public void onClick(View view) {
        //        Snackbar.make(view, date, Snackbar.LENGTH_LONG)
        //                .setAction("Action", null).show();
        //    }
        //});
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        //https://newsapi.org/v2/top-headlines?country=us&page=1&pageSize=1&apiKey=0b5e9f50873c4cad91e64f95034bd14c
        newsUrl = "https://newsapi.org/v2/top-headlines?country=us&page=1&pageSize=1&apiKey=0b5e9f50873c4cad91e64f95034bd14c";

    }
    @Override
    public void onPause() {
        super.onPause();
        //Toast.makeText(this, "IN PAUSE", Toast.LENGTH_SHORT).show();
        System.out.println();
        System.out.println();
        System.out.println("IN PAUSE");
        System.out.println();
        System.out.println();




        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Spinner spinner = (Spinner)findViewById(R.id.spinner1);
        //System.out.println("SAVING VALUE: "+SPINNER);
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        String s = String.valueOf(spinner.getSelectedItemPosition());

        System.out.println("SAVING VALUE: "+s);
        editor.putString(TEXT, s);
        editor.putString("Date", currentDateTimeString);
        editor.apply();

    }
    @Override
    public void onStop() {
        System.out.println();
        System.out.println();
        System.out.println("IN STOP");
        System.out.println();
        System.out.println();
        super.onStop();
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Toast.makeText(this, "IN STOP", Toast.LENGTH_SHORT).show();

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();



        Spinner spinner = (Spinner)findViewById(R.id.spinner1);
        //String s = String.valueOf(SPINNER);
        String s = String.valueOf(spinner.getSelectedItemPosition());
        System.out.println();
        editor.putString(TEXT, s);
        editor.putString("Date", currentDateTimeString);
        editor.apply();

    }

    @Override
    public void onStart() {
        super.onStart();
        /*
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String p = sharedPreferences.getString(TEXT, "");
        date = sharedPreferences.getString("Date", "");
        System.out.println();
        System.out.println();
        Toast.makeText(this, "FOUND VALUE: "+p, Toast.LENGTH_SHORT).show();
        System.out.println("FOUND VALUE: "+p);
        System.out.println();
        System.out.println();

        //pos = Integer.valueOf(p);
        Spinner spinner = (Spinner)findViewById(R.id.spinner1);
        int po = Integer.parseInt(p);
        spinner.setSelection(po);

        //setContentView(R.layout.content_search);
        View parentLayout = findViewById(R.id.fab);
        Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", null).show();


        /*
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, date, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        */
        doOnContinue();

    }

    @Override
    public void onResume() {
        super.onResume();
        doOnContinue();
        /*
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String p = sharedPreferences.getString(TEXT, "");
        date = sharedPreferences.getString("Date", "");
        System.out.println();
        System.out.println();
        //Toast.makeText(this, "FOUND VALUE: "+p, Toast.LENGTH_SHORT).show();
        System.out.println("FOUND VALUE: "+p);
        System.out.println();
        System.out.println();
        //pos = Integer.valueOf(p);

        Spinner spinner = (Spinner)findViewById(R.id.spinner1);
        int po = Integer.parseInt(p);
        spinner.setSelection(po);

        View parentLayout = findViewById(R.id.fab);
        Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", null).show();
*/
    }
    public void doOnContinue(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String p = sharedPreferences.getString(TEXT, "");
        date = sharedPreferences.getString("Date", "");
        System.out.println();
        System.out.println();
        Toast.makeText(this, "FOUND VALUE: "+p, Toast.LENGTH_SHORT).show();
        System.out.println("FOUND VALUE: "+p);
        System.out.println();
        System.out.println();
        if(p != "")
        {
            //pos = Integer.valueOf(p);
            Spinner spinner = (Spinner)findViewById(R.id.spinner1);
            int po = Integer.parseInt(p);
            spinner.setSelection(po);

            //setContentView(R.layout.content_search);
            View parentLayout = findViewById(R.id.fab);
            Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG)
                    .setAction("CLOSE", null).show();
        }

    }




    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        //Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
        newsUrl = "https://newsapi.org/v2/top-headlines?country=us&page=1&pageSize=1&category="+text+"&apiKey=0b5e9f50873c4cad91e64f95034bd14c";
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            openHome();
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void openHome()
    {
        Intent intent = new Intent(this, com.example.newsapp.HomePage.class);
        startActivity(intent);
    }
    public void search(View view) {
        //Toast.makeText(this, newsUrl, Toast.LENGTH_SHORT).show();
        resultsSearch();
        //new Search.AsyncHttpTask().execute(newsUrl);
    }
    public void resultsSearch()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SearchUrl", newsUrl);
        editor.apply();
        Intent intent = new Intent(this, com.example.newsapp.Result.class);
        startActivity(intent);
    }
    /*
    Button button = (Button) findViewById(R.id.button);
    button.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            // Do something in response to button click
        }
    });
    */
}

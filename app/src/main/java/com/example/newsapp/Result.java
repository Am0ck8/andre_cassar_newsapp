package com.example.newsapp;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Result extends AppCompatActivity {
    public static final String SHARED_PREFS = "sharedPrefs";
    public static String surl;
    TextView titleText;
    Button buttonAdd;

    DatabaseReference databaseTitles;
    ListView listViewArticles;

    List<ArticleTitle> articleTitleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        databaseTitles = FirebaseDatabase.getInstance().getReference("articles");

        titleText = (TextView) findViewById(R.id.articleTitle);
        buttonAdd = (Button) findViewById(R.id.saveButton);

        listViewArticles = (ListView) findViewById(R.id.listViewArticles);
        articleTitleList = new ArrayList<>();
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTitle();
            }
        });


    }
    private void addTitle(){

        String titleValue = titleText.getText().toString().trim();

        String id = databaseTitles.push().getKey();

        ArticleTitle article = new ArticleTitle(titleValue);
        databaseTitles.child(id).setValue(article);
        Toast.makeText(this, "Saving "+titleValue, Toast.LENGTH_LONG).show();
    }

    public class AsyncHttpTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                String response = streamToString(urlConnection.getInputStream());
                parseResult(response);
                return result;
            }

            catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }
    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferesReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while ((data = bufferesReader.readLine()) != null) {
            result += data;
        }

        if (null != stream) {
            stream.close();
        }

        return result;
    }
    //A

    void parseResult(String result) {
        JSONObject response = null;
        try {
            response = new JSONObject(result);
            JSONArray articles = response.optJSONArray("articles");

            for (int i = 0; i < articles.length(); i++) {

                JSONObject article = articles.optJSONObject(i);
                String title = article.optString("title");
                String des = article.optString("description");
                String img = article.optString("urlToImage");
                String url = article.optString("url");

                Log.i("Titles", title);
                TextView t = (TextView) findViewById(R.id.articleTitle);
                t.setText(title);
                //TextView editText2 = (TextView)findViewById(R.id.titlePointer);
                //editText2.setText("asdafsd", TextView.BufferType.EDITABLE);
                //editText2.setText("Hello world!");

                //rList.add(new RecyclerItem(R.drawable.ic_menu_share, title, des));
            }
        }

        catch (Exception e) {
            e.printStackTrace();
        }


    }
    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        surl = sharedPreferences.getString("SearchUrl", "");
        System.out.print("RESULT: "+surl);
        Toast.makeText(this, "FOUND VALUE: "+surl, Toast.LENGTH_LONG).show();
        new Result.AsyncHttpTask().execute(surl);

        databaseTitles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                articleTitleList.clear();
                for(DataSnapshot articleSnapshot : dataSnapshot.getChildren()){
                    ArticleTitle articleTitle = articleSnapshot.getValue(ArticleTitle.class);
                    articleTitleList.add(articleTitle);
                }
                ArticleList adapter = new ArticleList(Result.this, articleTitleList);
                listViewArticles.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        surl = sharedPreferences.getString("SearchUrl", "");
        System.out.print("RESULT: "+surl);
        Toast.makeText(this, "FOUND VALUE: "+surl, Toast.LENGTH_LONG).show();
        new Result.AsyncHttpTask().execute(surl);

        //pos = Integer.valueOf(p);


    }

}

package com.example.newsapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ArticleList extends ArrayAdapter<ArticleTitle> {
    private Activity context;
    private List<ArticleTitle> articleTitleList;

    public ArticleList(Activity context, List<ArticleTitle> articleTitleList){
        super(context, R.layout.list_layout, articleTitleList);
        this.context = context;
        this.articleTitleList = articleTitleList;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewtitle);

        ArticleTitle article = articleTitleList.get(position);

        textViewName.setText(article.getArticleTi());

        return listViewItem;
    }
}
